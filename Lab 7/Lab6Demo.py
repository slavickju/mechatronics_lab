# -*- coding: utf-8 -*-
'''
@file Lab6Demo.py

Runs ControlSystem class without recording results.

Created on Wed Nov 25 17:38:51 2020

@author: justi
'''

# %matplotlib
import matplotlib.pylab as plt
plt.figure()
plt.plot(range(100))