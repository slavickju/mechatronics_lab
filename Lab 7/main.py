'''
@file main.py

@brief A file that contains all of the classes and tasks required to execute
the motor control system.

@details This file contains the entire SISO closed-loop control system, and the 
task that executes it. It includes a MotorDriver class to run the motor duty 
cycle, the EncoderDriver class to read the shaft position and velocity, the 
ClosedLoop class to determine the velocity error, and the ControlSystem class
to calculate the motor duty cycle gain required. 

To use this class, just upload it to the Nucleo and the entire control system
will be able to run with this one file. It requires a single input asking for
the proportional control gain so that the ideal value can be best determined.
As it runs, it will print position and velocity positions to the REPL for 
storage. This data could be read by a different UI class to further analyze the
data. All system values can be changed in the task section of the file at the very
bottom.

This task differs from that of Lab 0x06 in that it is set to follow a specified
reference csv file. As of now, this file is unable to read new data from a csv
file as the reference velocity input had to be hard-coded in.

'''
import pyb
from pyb import UART
import utime
myuart = UART(2)


class ControlSystem:
    '''
    @brief This class allows one to update the gain from a given error and send
    the signal to the MotorDriver
    
    @details 
    '''
    
    def __init__ (self, mot, enc, loop):
        '''
        @brief Creates a Control System object, and specifies the specific 
        MotorDriver, EncoderDriver, and ControlLoop objects to be used.
        
        @details 

        Parameters
        ----------
        mot : TYPE
            DESCRIPTION.
        enc : TYPE
            DESCRIPTION.
        loop : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        self.motor = mot
        self.encoder = enc
        self.closed_loop = loop
        
        self.Kp = self.closed_loop.get_Kp()
        
        
        
        self.encoder_position1 = 0
        self.encoder_position2 = 0
        
        self.omega = 0
        
        self.forward = True
        
    def run (self):
        self.encoder.update()
        self.encoder_position1 = self.encoder_position2
        self.encoder_position2 = self.encoder.get_position()
        self.omega = 1e6*60*(self.encoder_position2 - self.encoder_position1)/((curr_time - start_time)*self.closed_loop.get_PPR())
        self.closed_loop.set_Omega_meas(self.omega)
        self.closed_loop.update()
        error = self.closed_loop.checkMax(self.motor.get_duty(),self.closed_loop.update())
        duty = self.motor.get_duty() + error
        self.motor.set_duty(duty, self.forward)
        self.motor.enable()
        
    def stop (self):
        self.motor.disable()

    def change_Kp (self, Kp):
        self.closed_loop.set_Kp(Kp)
        
    def change_Omega (self, omega, sign):
        self.closed_loop.set_Omega_ref(omega)
        self.forward = sign
        
    def change_PPR (self, PPR):
        self.encoder.set_PPR(PPR)
        
    def check_Omega (self):
        return self.omega
    
    def check_Position (self):
        return self.encoder.get_position()
    
  
class ClosedLoop:
    ''' 
    This class is in charge of defining the closed-loop transfer function, 
    changing the value of the controller, and saturation limits of the PWM. 
    '''    
    def __init__ (self, ref, init_gain, CPR):
        ''' 
        Creates a motor driver by initializing GPIO12pins and turning the motor 
        off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. 
        '''
        self.Kp = init_gain
        
        self.Omega_ref = ref
        self.Omega_meas = 0
        self.Omega_error = (self.Omega_ref - self.Omega_meas)*self.Kp
        
        self.PWM_max = 100
        self.PWM_min = 0
        
        self.CPR = CPR
        self.PPR = CPR*4
    
    def update (self):
        self.Omega_error = (self.Omega_ref - self.Omega_meas)*self.Kp
        return self.Omega_error
        
    def get_Kp (self):
        return self.Kp
        
    def set_Kp (self, Kp):
        self.Kp = Kp
        
    def get_PPR (self):
        return self.PPR
        
    def set_Omega_meas (self, omega):
        self.Omega_meas = omega
        
    def get_Omega_meas (self):
        return self.Omega_meas
        
    def set_Omega_ref (self, omega):
        self.Omega_ref = omega
        
    def get_Omega_ref (self):
        return self.Omega_ref
        
    def checkMax (self, duty, error):
        if (error+duty > self.PWM_max):
            error = self.PWM_max-duty
        if (error+duty < self.PWM_min):
            error = self.PWM_min-duty
        return error




class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 board. 
    '''    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' 
        Creates a motor driver by initializing GPIO12pins and turning the motor 
        off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. 
        '''
        self.sleep = nSLEEP_pin
        self.in1 = IN1_pin
        self.in2 = IN2_pin
        self.tim3 = timer
        self.t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.in1)
        self.t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.in2)
        self.duty = 0
        
    
    def enable (self):
        self.sleep.high()
        
    def disable (self):
        self.sleep.low()
        self.in1.low()
        self.in2.low()
        
    def set_duty (self, duty, ccw):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        self.duty = duty
        if (ccw):
            self.in2.low()
            self.t3ch1.pulse_width_percent(self.duty)
        else:
            self.in1.low()
            self.t3ch2.pulse_width_percent(self.duty)
        
        
    def get_duty (self):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        return self.duty
  
    
  
    
  
    
class EncoderDriver:
    
    ## Constant defining State 1
    S1_UPDATE  = 1    

    def __init__(self, pin1, pin2, timer, interval):
        '''
        @brief Creates a timer in encoder mode.
        
        @details Inputs two pin locations in the form of a string and a timer 
        in the form of an integer. This method uses these values to set up a 
        Timer in encoder mode using the specified int timer value. Also
        creates two stored_timer variables that store the two most recent
        updated timer counter values. There are two in order to compare the 
        delta and prevent overflow and underflow.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S1_UPDATE
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.tim = timer
        self.tim.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.position = 0
        
        self.PPR = 0
        
    # def run(self):
    #     self.curr_time = utime.ticks_us()
    #     if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
    #         if(self.state == self.S1_UPDATE):
    #             # Run State 1 Code
    #             if(Lab3Shares.CMD != None):
    #                 if(Lab3Shares.CMD == 0x7A):
    #                     self.set_position(0)  
    #                 elif(Lab3Shares.CMD == 0x70):
    #                     print(self.get_position())   
    #                 elif(Lab3Shares.CMD == 0x64):
    #                     print(self.get_delta())
    #                 else:
    #                     pass
                    
    #                 Lab3Shares.RESP = Lab3Shares.CMD
    #                 Lab3Shares.CMD = None
                    
    #             self.update()
                    
    #         else:
    #             # Invalid state code (error handling)
    #             pass
            
    #     self.runs += 1
            
    #     # Specifying the next time the task will run
    #     self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def update(self):
        '''
        @brief Updates the encoder's position and corrects for over/underflow.
        
        @details Changes the value of stored_timer to that currently outputted
        by the pyb Timer class's counter() method. Also checks for overflow
        or underflow. If the magnetude of delta is greater than half the 
        period, it corrects the value by adding or subtracting the period,
        depending on if delta is positive or negative.
        '''
        self.stored_timer2 = self.stored_timer1
        self.stored_timer1 = self.tim.counter()
        if (abs(self.get_delta()) > 0xFFFF/2):
            if(self.get_delta() < -0xFFFF/2):
                self.position = self.position + self.set_delta(0xFFFF)
            else:
                self.position = self.position + self.set_delta(-0xFFFF)
        else:
            self.position = self.position + self.get_delta()
        
    def get_position(self):
        '''
        @brief Gets the encoder's most recent stored position.
        
        @details Returns the value of stored_timer1.
        '''
        # return self.position
        return self.position

    def set_position(self, position):
        '''
        @brief Changes the encoder's position and updates the stored_encoder
        value
        
        @details Changes the value of pyb Timer's counter method to the value 
        given in position. It then calls the update method to update the
        stired timer value. Mostly used to set the encoder position to zero.
        It also resets both stored_timer values to avoid outputting a delta
        due to recalibration.
        '''
        self.tim.counter(position)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.update()
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent stored
        timer values
        
        @details Finds the difference between stored_timer1 and stored_timer2
        in order to find if there is. overflow or underflow in the encoder.
        This value is returned
        '''
        return (self.stored_timer1 - self.stored_timer2)
    
    def set_delta(self, value):
        '''
        @brief Changes the delta to a different value
        
        @details Returns delta added with another value initialized in the 
        method. The main use of this is to correct "bad" delta values by
        adding or subtracting the period.
        '''
        return (self.get_delta() + value)
    
    def set_PPR(self, PPR):
        self.PPR = PPR
        


# 
# import utime
# myuart = UART(2)

# interval = 200000   
# start_time = utime.ticks_us()     
# ## The "timestamp" for when the task should run next
# next_time = utime.ticks_add(start_time, interval)


# cs = None

# while (cs == None):
#     if (myuart.any() != 0):
#         Kp = float(myuart.readChar())
#         closed_loop = ClosedLoop(100, 0.5, 1000)
#         cs = ControlSystem(motor, encoder, closed_loop)
        
# for n in range(1,100):
#     cs.run()
        

# pin1 = pyb.Pin.cpu.B6
# pin2 = pyb.Pin.cpu.B7
# timer = 3
# tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
# myEncoder = EncoderDriver(pin1, pin2, timer, interval)
# tim4.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
# tim4.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)

# collect_data = False
# array_length = -1
# start_list_time = utime.ticks_us()

# S0_INIT = 0
# S1_READ_INPUT = 1
# S2_RECORD_DATA = 2
# state = S0_INIT





# from pyb import UART
# myuart = UART(2)
# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
#         myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo')

if (__name__ =='__main__'):
    sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    tim3 = pyb.Timer(3, freq=20000)
    motor = MotorDriver(sleep, in1, in2, tim3)

    pin1 = pyb.Pin.cpu.B6
    pin2 = pyb.Pin.cpu.B7
    tim4 = pyb.Timer(4, freq=20000)
    tim4.period(0xFFFF)
    encoder = EncoderDriver(pin1, pin2, tim4, 200000)
    
    omega_ref = 0 #RPM
    Kp = 0.001
    CPR = 1000

    closed_loop = ClosedLoop(omega_ref, Kp, CPR)
    cs = ControlSystem(motor, encoder, closed_loop)
    cs.stop()
    
    S0_INIT = 0
    S1_READ_INPUT = 1
    S2_RECORD_DATA = 2
    
    state = S0_INIT
    
    ## Counter that describes the number of times the task has run
    runs = 0
        
    ##  The amount of time in seconds between runs of the task
    interval = 100000

    # # Blank lists for data. On the Nucleo you should probably use array.array() to
    # # better storage density. Make sure you pick the right data type when you init
    # # the array if you do go with array.array instead of a list.
    # time = []
    # velocity = []
    # position = []
    # i = 0

    # # Open the file. From here it is essentially identical to serial comms. There
    # # are better ways to do this, like using the 'with' statement, but using an
    # # object like this is closest to the serial code you've already used.
    # ref = open('reference.csv');

    # # Read data indefinitely. Loop will break out when the file is done.
    # #
    # # You will need to handle this slightly differently on the Nucleo or
    # # pre-process the provided data, because there are 15,001 rows of data in the
    # # CSV which will be too much to store in RAM on the Nucleo.
    # #
    # # Consider resampling the data, interpolating the data, or manipulating the 
    # # data in some fashion to limit the number of rows based on the 'interval' for
    # # your control task. If your controller runs every 20ms you do not need the
    # # data sampled at 1ms provided by the file.
    # #
    # while True:
    #     # Read a line of data. It should be in the format 't,v,x\n' but when the
    #     # file runs out of lines the lines will return as empty strings, i.e. ''
    #     line = ref.readline()
    
    #     # If the line is empty, there are no more rows so exit the loop
    #     if line == '':
    #         break
    
    #     # If the line is not empty, strip special characters, split on commas, and
    #     # then append each value to its list.
    #     else:
    #         (t,v,x) = line.strip().split(',');
    #         # time.append(float(t))
    #         velocity.append(float(v))
    #         # position.append(float(x))
    #         i += 1

    # # Can't forget to close the serial port
    # ref.close()
    
    ## The timestamp for the first iteration
    start_time = utime.ticks_us()
        
    ## The "timestamp" for when the task should run next
    next_time = utime.ticks_add(start_time, interval)
    
    collect_data = False
    
    start_list_time = start_time
    
    omega_vector = []
    i = 0
    
    while (True):
        curr_time = utime.ticks_us()
        if (utime.ticks_diff(curr_time, next_time) >= interval):
            if (state == S0_INIT):
                state = S1_READ_INPUT
        
            if (state == S1_READ_INPUT):
                if (myuart.any() != 0):
                    Kp = myuart.readchar()
                    # myuart.write('Started Data Collection ')
                    cs.change_Kp(Kp)
                    collect_data = True
                    start_list_time = curr_time
                    runs = 0
               
                if (utime.ticks_diff(curr_time, start_list_time) >= 15e6):
                    start_list_time = curr_time
                    runs = 0
                    collect_data = False
                    cs.stop()
                    while(runs < i):
                        myuart.write(str(omega_vector.pop(0)) + ',')
                        runs += 1
                    runs = 0
                    i = 0
                    
                if(collect_data): 
                    state = S2_RECORD_DATA
                else:
                    runs = 0
        
            if (state == S2_RECORD_DATA):
                if (utime.ticks_diff(curr_time,start_list_time) < 4e6):
                    cs.change_Omega(30, True)
                elif (utime.ticks_diff(curr_time,start_list_time) < 7e6):
                    cs.change_Omega(0, True)
                elif (utime.ticks_diff(curr_time,start_list_time) < 8e6):
                    cs.change_Omega(60, False)
                elif (utime.ticks_diff(curr_time,start_list_time) < 11e6):
                    cs.change_Omega(0, True)
                elif (utime.ticks_diff(curr_time,start_list_time) < 12e6):
                    ramp_time = utime.ticks_us()
                    cs.change_Omega((30*(ramp_time-11e6)/1e6), True)
                elif (utime.ticks_diff(curr_time,start_list_time) < 12e6):
                    ramp_time = utime.ticks_us()
                    cs.change_Omega((30*(1-((ramp_time-11e6)/1e6))), True)
                else:
                    cs.change_Omega(0, True)
                cs.run()
                myuart.write(str(cs.check_Position()*360/4000)+',')
                omega_vector.append(cs.check_Omega())
                i += 1
                # myuart.write(str(tim4.counter())+',')
                # print(str(utime.ticks_diff(curr_time, start_list_time)) + '    ')
                state = S1_READ_INPUT
            
            start_time = curr_time
            
            runs += 1
            
            # Specifying the next time the task will run
            next_time = utime.ticks_add(next_time, interval)
    