# -*- coding: utf-8 -*-
'''
@file UIFrontEnd.py

This file reads commands from the console and writes them to the Nucleo for it
to record encoder data. It then reads the results from the Nucleo and creates
a plot and a CSV file of the data.

@author: justi
'''

import serial
import array
import time
import csv
import matplotlib.pyplot as plt
import numpy as np

# Blank lists for data. On the Nucleo you should probably use array.array() to
# better storage density. Make sure you pick the right data type when you init
# the array if you do go with array.array instead of a list.
time_array_2 = []
velocity = []
position = []

# Open the file. From here it is essentially identical to serial comms. There
# are better ways to do this, like using the 'with' statement, but using an
# object like this is closest to the serial code you've already used.
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.
#
# You will need to handle this slightly differently on the Nucleo or
# pre-process the provided data, because there are 15,001 rows of data in the
# CSV which will be too much to store in RAM on the Nucleo.
#
# Consider resampling the data, interpolating the data, or manipulating the 
# data in some fashion to limit the number of rows based on the 'interval' for
# your control task. If your controller runs every 20ms you do not need the
# data sampled at 1ms provided by the file.
#
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',');
        time_array_2.append(float(t))
        velocity.append(float(v))
        position.append(float(x))

# Can't forget to close the serial port
ref.close()

# To make the example complete, plot the data as a subplot.
plt.figure(1)

# # Velocity first
# plt.subplot(2,1,1)
# plt.plot(time,velocity)
# plt.ylabel('Velocity [RPM]')

# # Then position
# plt.subplot(2,1,2)
# plt.plot(time,position)
# plt.xlabel('Time [s]')
# plt.ylabel('Position [deg]')

# fig = plt.figure()
# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     return myval

# for n in range(1):
#     print(sendChar())
    
# ser.close()

S0_INIT = 0
S1_LIST_OF_COMMANDS = 1
S2_USER_INPUT = 2
S3_ENCODER_RESPONSE = 3
state = S0_INIT
runs = 0
Proceed = True
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
writer = csv.writer(open('Lab6Data.csv', 'w', newline=''))

while(Proceed):
    if(state == S0_INIT):
        my_arr = array.array('f', [])
        data = array.array('f', [])
        time_array = array.array('f', []);
        vel_array = array.array('f', [])
        ## Initializes the serial port
        #self.ser.init(9600, bits=8, parity=None, stop=1)
        state = S1_LIST_OF_COMMANDS
                 
    elif(state == S1_LIST_OF_COMMANDS):
        # Run State 1 Code
        print('List of Commands: ')
        print('G: Start Control System')
        print('S: Stop Control System')
        print('Q: Quit')
        print(' ')
        inv = input('Enter your Command: ')
        if (inv == 'G'):
            print(' ')
            Kp = input('Enter Kp Value: ')
            state = S2_USER_INPUT
        elif (inv == 'S'):
            state = S3_ENCODER_RESPONSE
        elif (inv == 'Q'):
            Proceed = False
        else:
            print('Invalid Response, Try Again')
            print(' ')
        #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
    elif(state == S2_USER_INPUT):
        # Run State 2 Code
        ser.write(str(Kp).encode('ascii'))
        state = S1_LIST_OF_COMMANDS
        
    elif(state == S3_ENCODER_RESPONSE):
        # Run State 3 Code
        if (inv == 'S'):
            myval = ser.readline().decode('ascii')
            my_arr = myval.split(',')
            print(my_arr)
            array_size = my_arr.index('')
            my_arr.pop()
            for i in range(1,int(array_size/2)):
                data.append(float(my_arr.pop(0)))
                time_array.append(i/10)
                vel_array.append(float(my_arr.pop()))
            vel_array.reverse()
            data = data.tolist()
            time_array = time_array.tolist()
            fig = plt.figure()
            ax1 = fig.add_subplot(211)
            ax1.set_xlabel('Time (s)')
            ax1.set_title('Control System Position Response')
            ax1.plot(time_array_2,position, 'r')
            ax1.set_ylabel('Position [theta]')
            ax1.plot(time_array, data)
            plt.ylim(0,810)
            ax2 = fig.add_subplot(212)
            ax2.set_xlabel('Time (s)')
            ax2.set_title('Control System Velocity Response')
            ax2.plot(time_array_2,velocity, 'r')
            ax2.set_ylabel('Velocity [RPM]')
            ax2.plot(time_array, vel_array)
            plt.ylim(-80,80)
            time_stamp = time.ctime()
            data.insert(0, time_stamp)
            print(data)
            writer.writerow(data)
            
        state = S0_INIT
        # if (myval != ''):
        #     my_arr.append(int(myval))
        #     time += (1/5)
        #     time_array.append(time)
        # if (inv == 'S'):
        #     fig, ax = plt.subplots()  # Create a figure containing a single axes.
        #     ax.plot(time_array, my_arr)  # Plot some data on the axes.
        #     my_arr = array.array('I', [])
        #     time_array = array.array('I', [])
        #     time = 0;
        # state = S1_LIST_OF_COMMANDS
        # fig, ax = plt.subplots()
        
        
        # if (myval != ''):
        #     state = S1_LIST_OF_COMMANDS
        # else:
        #     state = S3_ENCODER_RESPONSE
        
        
    runs += 1
            
        

                
        
