'''
@file MotorDriver.py

Created on Tue Nov 24 13:26:40 2020

@author: justi
'''
import pyb

class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 board. 
    '''    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' 
        Creates a motor driver by initializing GPIO12pins and turning the motor 
        off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. 
        '''
        print ('Creating a motor driver')
        self.sleep = nSLEEP_pin
        self.in1 = IN1_pin
        self.in2 = IN2_pin
        self.tim3 = timer
        self.t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.in1)
        self.t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.in2)
        self.duty = 0
        
    
    def enable (self):
        print ('Enabling Motor')
        self.sleep.high()
        
    def disable (self):
        print ('Disabling Motor')
        self.sleep.low()
        self.in1.low()
        self.in2.low()
        
    def set_duty (self, duty, ccw):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        self.duty = duty
        if (ccw):
            self.in2.low()
            self.t3ch1.pulse_width_percent(self.duty)
        else:
            self.in1.low()
            self.t3ch2.pulse_width_percent(self.duty)
        
        
    def get_duty (self):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        return self.duty
    
    
if (__name__ =='__main__'):
    # Adjust the following code to write a test program for your motor 
    # class. Any code within the if __name__ == '__main__' block will 
    # only run when the script is executed as a standalone program. If 
    # the script is imported as a module the code block will not run.
            
    # Create the pin objects used for interfacing with the motor driver
    sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    tim3 = pyb.Timer(3, freq=20000)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(sleep, in1, in2, tim3)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(50, True)
            
            
            
            
            
            
            
            
            
            
            