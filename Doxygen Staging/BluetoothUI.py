
'''
@file BluetoothUI.py

Creates a BLEModuleDriver object and uses it and commands from the DSD TECH
app to cause the user LED to flicker at user specified frequencies.

Created on Tue Nov 24 01:45:41 2020

@author: justi
'''

import pyb
import utime
from BLEModuleDriver.py import BLEModuleDriver

## Counter that describes the number of times the task has run
runs = 0
        
##  The amount of time in seconds between runs of the task
interval = 1e6
        
## The timestamp for the first iteration
start_time = utime.ticks_us()
        
## The "timestamp" for when the task should run next
next_time = utime.ticks_add(start_time, interval)
        
pinA5 = pyb.Pin.cpu.A5
driver = BLEModuleDriver(pinA5)

while(True):
    curr_time = utime.ticks_us()
    if (utime.ticks_diff(curr_time, next_time) >= 0):
        if(driver.checkChar()):
            val = driver.readChar()
            if (val != None):
                flicker = True
                flic_time = curr_time
        if (flicker and (utime.ticks_diff(flic_time,curr_time) > 1e6/val)):
            if (driver.isOn()):
                driver.turnOff()
            else:
                driver.turnOn()
            
        runs += 1
            
        # Specifying the next time the task will run
        next_time = utime.ticks_add(next_time, interval)

