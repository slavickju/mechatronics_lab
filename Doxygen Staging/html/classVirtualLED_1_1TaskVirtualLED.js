var classVirtualLED_1_1TaskVirtualLED =
[
    [ "__init__", "classVirtualLED_1_1TaskVirtualLED.html#a0632cece9ea06745697ed5be0f9d2bee", null ],
    [ "run", "classVirtualLED_1_1TaskVirtualLED.html#a85e7cc340a24dfc14236f0830cc00e08", null ],
    [ "transitionTo", "classVirtualLED_1_1TaskVirtualLED.html#ab0b9297057ab9ce5dbf503c9b70e082e", null ],
    [ "curr_time", "classVirtualLED_1_1TaskVirtualLED.html#adeaf87df2426f4238fc9e122ac1bfb7b", null ],
    [ "interval", "classVirtualLED_1_1TaskVirtualLED.html#aa9b7004aa11dc1e7b98144cfda00fc0a", null ],
    [ "next_time", "classVirtualLED_1_1TaskVirtualLED.html#a89dad39ed94c3a1125a7006a24c85435", null ],
    [ "runs", "classVirtualLED_1_1TaskVirtualLED.html#a7afc0188308d920566b614f6355f13d2", null ],
    [ "start_time", "classVirtualLED_1_1TaskVirtualLED.html#a7b224d30c4b1b9ad7b4e9422c505ee8c", null ],
    [ "state", "classVirtualLED_1_1TaskVirtualLED.html#ad96c7682f6d9a876713293b585f236c5", null ]
];