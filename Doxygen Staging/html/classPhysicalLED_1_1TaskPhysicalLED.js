var classPhysicalLED_1_1TaskPhysicalLED =
[
    [ "__init__", "classPhysicalLED_1_1TaskPhysicalLED.html#a32d19735c5c471eff50bc6a6e67d71ae", null ],
    [ "run", "classPhysicalLED_1_1TaskPhysicalLED.html#a25520bf7f3feb1842acd4ec67a173576", null ],
    [ "transitionTo", "classPhysicalLED_1_1TaskPhysicalLED.html#acb10403711f0113929af0013c974de60", null ],
    [ "curr_time", "classPhysicalLED_1_1TaskPhysicalLED.html#ad2029fc69b5bfdea21fca6861060292d", null ],
    [ "interval", "classPhysicalLED_1_1TaskPhysicalLED.html#a5a4212ebf2fcce805d5d4cda2d413e84", null ],
    [ "next_time", "classPhysicalLED_1_1TaskPhysicalLED.html#a633e417f3b1d1485e143ae037f989da1", null ],
    [ "pinA5", "classPhysicalLED_1_1TaskPhysicalLED.html#ad4476278f78b38e9abd18ac860eef9ff", null ],
    [ "runs", "classPhysicalLED_1_1TaskPhysicalLED.html#ac4c759d2c2c58e8e8e13bce6099f16a9", null ],
    [ "start_time", "classPhysicalLED_1_1TaskPhysicalLED.html#a14356428351e1492c9b8f50e2e80f7a0", null ],
    [ "state", "classPhysicalLED_1_1TaskPhysicalLED.html#aba81d3279bca7e046697447ad8fdbeba", null ],
    [ "t2ch1", "classPhysicalLED_1_1TaskPhysicalLED.html#a5d3558481b0db671c261ecf0dfcfae8f", null ],
    [ "tim2", "classPhysicalLED_1_1TaskPhysicalLED.html#a2e64312cc5a6d7787a51a1b9c37d8a61", null ]
];