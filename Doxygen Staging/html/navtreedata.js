/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Directory", "index.html#sec_directory", null ],
    [ "Fibonacci (Lab 0x01)", "page_fib.html", [
      [ "Description", "page_fib.html#page_fib_desc", null ],
      [ "Source Code Access", "page_fib.html#page_fib_src", null ],
      [ "Documentation", "page_fib.html#page_fib_doc", null ]
    ] ],
    [ "Elevator State System (HW 0x00)", "page_StateSys.html", [
      [ "Description", "page_StateSys.html#page_ss_desc", null ],
      [ "Source Code Access", "page_StateSys.html#page_ss_src", null ]
    ] ],
    [ "Blinking LEDs (Lab 0x02)", "page_LED.html", [
      [ "Description", "page_LED.html#page_led_desc", null ],
      [ "Source Code Access", "page_LED.html#page_led_src", null ],
      [ "Documentation", "page_LED.html#page_led_doc", null ]
    ] ],
    [ "Incrimental Encoders (Lab 0x03)", "page_enc.html", [
      [ "Description", "page_enc.html#page_enc_desc", null ],
      [ "Source Code Access", "page_enc.html#page_enc_src", null ],
      [ "Documentation", "page_enc.html#page_enc_doc", null ]
    ] ],
    [ "Nucleo UI FrontEnd (Lab 0x04)", "page_uife.html", [
      [ "Description", "page_uife.html#page_uife_desc", null ],
      [ "Source Code Access", "page_uife.html#page_uife_src", null ],
      [ "Documentation", "page_uife.html#page_uife_doc", null ]
    ] ],
    [ "Bluetooth UI (Lab 0x05)", "page_app.html", [
      [ "Description", "page_app.html#page_app_desc", null ],
      [ "Source Code Access", "page_app.html#page_app_src", null ],
      [ "Documentation", "page_app.html#page_app_doc", null ]
    ] ],
    [ "Control System (Lab 0x06)", "page_ctrl.html", [
      [ "Description", "page_ctrl.html#page_ctrl_desc", null ],
      [ "Source Code Access", "page_ctrl.html#page_ctrl_src", null ],
      [ "Documentation", "page_ctrl.html#page_ctrl_doc", null ]
    ] ],
    [ "Reference Tracker (Lab 0x07)", "page_trac.html", [
      [ "Description", "page_trac.html#page_trac_desc", null ],
      [ "Source Code Access", "page_trac.html#page_trac_src", null ],
      [ "Documentation", "page_trac.html#page_trac_doc", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
""
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';