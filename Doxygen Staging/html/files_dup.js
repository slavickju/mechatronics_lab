var files_dup =
[
    [ "BLEModuleDriver.py", "BLEModuleDriver_8py.html", [
      [ "BLEModuleDriver", "classBLEModuleDriver_1_1BLEModuleDriver.html", "classBLEModuleDriver_1_1BLEModuleDriver" ]
    ] ],
    [ "BluetoothUI.py", "BluetoothUI_8py.html", "BluetoothUI_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "EncoderDriver", "classEncoder_1_1EncoderDriver.html", "classEncoder_1_1EncoderDriver" ],
      [ "EncoderInterface", "classEncoder_1_1EncoderInterface.html", "classEncoder_1_1EncoderInterface" ]
    ] ],
    [ "Fibonacci.py", "Fibonacci_8py.html", "Fibonacci_8py" ],
    [ "Homework0x00.py", "Homework0x00_8py.html", [
      [ "TaskElevator", "classHomework0x00_1_1TaskElevator.html", "classHomework0x00_1_1TaskElevator" ],
      [ "Button", "classHomework0x00_1_1Button.html", "classHomework0x00_1_1Button" ],
      [ "MotorDriver", "classHomework0x00_1_1MotorDriver.html", "classHomework0x00_1_1MotorDriver" ]
    ] ],
    [ "Homework0x00main.py", "Homework0x00main_8py.html", "Homework0x00main_8py" ],
    [ "Lab0x02_main.py", "Lab0x02__main_8py.html", "Lab0x02__main_8py" ],
    [ "Lab0x03_main.py", "Lab0x03__main_8py.html", "Lab0x03__main_8py" ],
    [ "Lab3Shares.py", "Lab3Shares_8py.html", "Lab3Shares_8py" ],
    [ "Lab5Demo.py", "Lab5Demo_8py.html", "Lab5Demo_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "motor.py", "motor_8py.html", [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "mytest.py", "mytest_8py.html", null ],
    [ "PhysicalLED.py", "PhysicalLED_8py.html", [
      [ "TaskPhysicalLED", "classPhysicalLED_1_1TaskPhysicalLED.html", "classPhysicalLED_1_1TaskPhysicalLED" ]
    ] ],
    [ "UIFrontEnd.py", "UIFrontEnd_8py.html", "UIFrontEnd_8py" ],
    [ "VirtualLED.py", "VirtualLED_8py.html", [
      [ "TaskVirtualLED", "classVirtualLED_1_1TaskVirtualLED.html", "classVirtualLED_1_1TaskVirtualLED" ]
    ] ]
];