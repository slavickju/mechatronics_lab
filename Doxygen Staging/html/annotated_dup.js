var annotated_dup =
[
    [ "BLEModuleDriver", null, [
      [ "BLEModuleDriver", "classBLEModuleDriver_1_1BLEModuleDriver.html", "classBLEModuleDriver_1_1BLEModuleDriver" ]
    ] ],
    [ "Encoder", null, [
      [ "EncoderDriver", "classEncoder_1_1EncoderDriver.html", "classEncoder_1_1EncoderDriver" ],
      [ "EncoderInterface", "classEncoder_1_1EncoderInterface.html", "classEncoder_1_1EncoderInterface" ]
    ] ],
    [ "Homework0x00", null, [
      [ "Button", "classHomework0x00_1_1Button.html", "classHomework0x00_1_1Button" ],
      [ "MotorDriver", "classHomework0x00_1_1MotorDriver.html", "classHomework0x00_1_1MotorDriver" ],
      [ "TaskElevator", "classHomework0x00_1_1TaskElevator.html", "classHomework0x00_1_1TaskElevator" ]
    ] ],
    [ "motor", "namespacemotor.html", "namespacemotor" ],
    [ "PhysicalLED", null, [
      [ "TaskPhysicalLED", "classPhysicalLED_1_1TaskPhysicalLED.html", "classPhysicalLED_1_1TaskPhysicalLED" ]
    ] ],
    [ "VirtualLED", null, [
      [ "TaskVirtualLED", "classVirtualLED_1_1TaskVirtualLED.html", "classVirtualLED_1_1TaskVirtualLED" ]
    ] ]
];