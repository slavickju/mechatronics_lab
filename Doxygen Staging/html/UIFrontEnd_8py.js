var UIFrontEnd_8py =
[
    [ "array_size", "UIFrontEnd_8py.html#abbf4d6637983fd2847ae7f7f05c4ec0f", null ],
    [ "data", "UIFrontEnd_8py.html#addfb208711f2488f8b3efd4b334b9c69", null ],
    [ "inv", "UIFrontEnd_8py.html#a90e1adf5250f9c8b50903ca0e4c22a8b", null ],
    [ "my_arr", "UIFrontEnd_8py.html#a5aade0b7f7e887a87ffb52fdaa34159e", null ],
    [ "myval", "UIFrontEnd_8py.html#a818b4fbd36ebabad5cb0781e901a3c0c", null ],
    [ "runs", "UIFrontEnd_8py.html#a891f8e7ee464d2ae221c5a76c081d9f2", null ],
    [ "S0_INIT", "UIFrontEnd_8py.html#a7f8da91fe10189a8d4180d883076d355", null ],
    [ "S1_LIST_OF_COMMANDS", "UIFrontEnd_8py.html#ab7ca3c5f0bd3311c93bbaa9aaf831f0a", null ],
    [ "S2_USER_INPUT", "UIFrontEnd_8py.html#a27342434aea997e290d7df9b5b1e0a6b", null ],
    [ "S3_ENCODER_RESPONSE", "UIFrontEnd_8py.html#af80de74710eb0f3dfa34bd7a8236bccc", null ],
    [ "ser", "UIFrontEnd_8py.html#a28f4f90742b57b1d398685d67b471239", null ],
    [ "state", "UIFrontEnd_8py.html#a012d1521317b96f34bb1e09dd6a2f080", null ],
    [ "time_array", "UIFrontEnd_8py.html#a0344f42ef27f8da4386790733825d59f", null ],
    [ "time_stamp", "UIFrontEnd_8py.html#a462e3c25f57c78504d19cfabe7c5c5ce", null ],
    [ "writer", "UIFrontEnd_8py.html#a7483940abf0b6df60521938c321a2107", null ]
];