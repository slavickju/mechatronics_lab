var main_8py =
[
    [ "array_length", "main_8py.html#a34ffa0861202c751fe81291df66cb7d5", null ],
    [ "collect_data", "main_8py.html#aee15b339fcfa70ed03931345f2143515", null ],
    [ "curr_time", "main_8py.html#af0fea2d37afc938ca5ccd6bc7198165f", null ],
    [ "interval", "main_8py.html#a86060447ac22c1b649ef0497e8adf1ba", null ],
    [ "mode", "main_8py.html#a91270e2576422db8bc835d337b054654", null ],
    [ "myuart", "main_8py.html#a9be3fc12d7dd7ca0504e230b84d96936", null ],
    [ "next_time", "main_8py.html#a97359d1c36ca6a548999bed09f02f11a", null ],
    [ "pin", "main_8py.html#ad0aeeee1d0309da156beb6b349bae798", null ],
    [ "pin1", "main_8py.html#aa1ce508fc8a3ae7790d479ccc2511299", null ],
    [ "pin2", "main_8py.html#af843192661862089e909e4f3d62e1f5e", null ],
    [ "S0_INIT", "main_8py.html#a02f68b84e6c5f30faa5bd7aa7a99bf3f", null ],
    [ "S1_READ_INPUT", "main_8py.html#ae9a45832083458c1ddd7b73a8d329fdd", null ],
    [ "S2_RECORD_DATA", "main_8py.html#a7d16b5a4e4db138c555b481ec9739627", null ],
    [ "start_list_time", "main_8py.html#a0fc4dc0e2be718420dc84c0db447b4d8", null ],
    [ "start_time", "main_8py.html#ae57958345b17f9ca8597330ba07e1a1c", null ],
    [ "state", "main_8py.html#a930442dd832ba9fe94227723c47f6653", null ],
    [ "tim", "main_8py.html#a232f417b17e7cf7003f080763c8f511f", null ],
    [ "timer", "main_8py.html#a4fe20a9d36fdc5b4ea50d88f43f6ed54", null ],
    [ "val", "main_8py.html#af30a285a2222c0a986b33b478f9bf927", null ]
];