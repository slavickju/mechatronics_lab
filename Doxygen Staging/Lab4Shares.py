# -*- coding: utf-8 -*-
'''
@file Lab3Shares.py

Created on Thu Oct 22 02:10:10 2020

@author: justi
'''

## Command variable that sends signal from interface to driver (int)
CMD = None;

## Response variable that sends confirmation signal back to interface (int)
DATA = None;

TIME = None;