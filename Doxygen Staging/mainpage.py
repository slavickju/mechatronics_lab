'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
This website hosts documentation related to Justin Slavick's ME305 repository located 
at:
    https://bitbucket.org/slavickju/mechatronics_lab/src/master/
    
This portfolio contains information on the seven labs and one homework
assignment that have been completed as part of the Fall 2020 curriculum of ME
305 at Cal Poly. Below, you will find links to each of the pages representing
a brief overview of the program, links to the code sources, and references to 
each of the individual file descriptions. 

@section sec_directory Directory
1. \ref page_StateSys
2. \ref page_fib
3. \ref page_LED
4. \ref page_enc
5. \ref page_uife
6. \ref page_app
7. \ref page_ctrl
8. \ref page_trac

@page page_fib Fibonacci (Lab 0x01)

@section page_fib_desc Description
The Fibonacci source code allows one to calculate the value of a number in 
the Fibonacci sequence given the index. 

@section page_fib_src Source Code Access
Source Code related to Lab 0x01 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%201/

@section page_fib_doc Documentation
Documentation related to Lab 0x01 is located at \ref Fibonacci.py

@page page_StateSys Elevator State System (HW 0x00)

@section page_ss_desc Description
The elevator state system source code allows for the operation of two
independant elevator system operating between two floor. The user can provide
input through floor buttons located on each floor. In addition, each floor
has a proximity sensor that indicates the presence of the elevator on that
floor. Finally, there is a single motor capable of driving the elevator up
or down in the system. The state diagram is provided below.

@image html StateTransitionDiagram.JPG width=50%

@section page_ss_src Source Code Access
Source Code related to HW 0x00 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Homework%200/

@section page_fib_doc Documentation
Documentation related to HW 0x00 is located at \ref Homework0x00.py and \ref Homework0x00main.py

@page page_LED Blinking LEDs (Lab 0x02)

@section page_led_desc Description
The Blinking LED source code allows for the simultanious blinking of a virtual
LED (text in terminal), and a physical LED on the Nucleo control board. The
virtual LED prints ON and OFF in an alternating pattern every second, while
the physical LED changes from 0% to 100% brightness in an alternating wave
pattern with a wavelength of 10 seconds. It alternates between a sinusoidal 
and a triangle wave pattern every 30 seconds.

@section page_led_src Source Code Access
Source Code related to Lab 0x02 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%202/

@section page_led_doc Documentation
Documentation related to Lab 0x02 is located at \ref VirtualLED.py, \ref PhysicalLED.py, and \ref Lab0x02_main.py

@page page_enc Incrimental Encoders (Lab 0x03)

@section page_enc_desc Description
The Incrimental Encoders source code provides the user a way to measure the
position of a motor using the encoder timer. The \ref Encoder.py file contains
two classes: an EncoderDriver class that calculates encoder position and
corrects for over/underflow, and an EncoderInterface class that allows the
user to interface with the Nucleo using simple one-character commands.
The \ref Lab3Shares.py file allows multiple tasks to communicate with one
another by housing global variables.

@section page_enc_src Source Code Access
Source Code related to Lab 0x03 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%203/

@section page_enc_doc Documentation
Documentation related to Lab 0x03 is located at \ref Encoder.py, \ref Lab3Shares.py, and \ref Lab0x03_main.py

@page page_uife Nucleo UI FrontEnd (Lab 0x04)

@section page_uife_desc Description
The Nucleo UI FrontEnd source code allows one to record encoder data stored on
the Nucleo through Spyder. It includes \ref UIFrontEnd.py code to execute in
Spyder and prompt user commands and \ref main.py code that execultes on the Nucleo. 
It also outputs a plot and CSV file of the data once finished. \ref main.py also
contains a copy of the EncoderDriver class within it for ease of use.

@image html EncoderPositionPlot.png

@image html Lab4FSM.JPG width=50%

@image html Lab4TaskDiagram.JPG width=50%

@section page_uife_src Source Code Access
Source Code related to Lab 0x04 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%204/

@section page_uife_doc Documentation
Documentation related to Lab 0x04 is located at \ref UIFrontEnd.py and \ref main.py

@page page_app Bluetooth UI (Lab 0x05)

@section page_app_desc Description
The Bluetooth UI source code allows one to flicker the Nucleo user LED using a
phone app. It includes one \ref BluetoothUI.py file that contains both a
BLEModuleDriver class, that interprets values recieved over bluetooth and
controls whether or not the Nucleo LED turns on or not, and a UI task that
prompts values from the phone, sends updates to the phone, and constructs the
BLEModuleDriver object. This has been done for ease of use.

@image html Lab0x05FSM.JPG width=50%

@section page_app_src Source Code Access
Source Code related to Lab 0x05 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%205/

@section page_app_doc Documentation
Documentation related to Lab 0x05 is located at \ref BluetoothUI.py and \ref BLEModuleDriver.py

@page page_ctrl Control System (Lab 0x06)

@section page_ctrl_desc Description
This lab utilizes an SISO closed-loop control system with a positional controller
to monitor the step response of the motor. \ref main.py contains many classes 
that allow this to work such as MotorDriver, which is responsible for powering
and changing the motor speed, EncoderDriver, which is responsible for recording
the shaft position/velocity and storing the data, ClosedLoop, which is
responsible for calculating the error in measured and reference velocities, and
ControlSystem, which uses the error to send a controlled signal to the
MotorDriver. \ref main.py also executes the Nucleo task of recieving signals
from Spyder, storing and organizing data from the encoder into lists, and
creating each of the objects and known values. \ref UIFrontEnd.py is a user
interface that recieves prompted commands and presents the data from \ref 
main.py into a plot and a csv file.

Unfortunately, there is a lot of friction on both motors, leading to an inability
for the motor to spin unless an extra torque is provided on high duty cycles. This
is the reason why the code produces large oscillations regardless of the Kp value
used.

@image html ControlSystemResponse1.png

@image html ControlSystemResponse2.png

@image html ControlSystemResponse3.png

@image html ControlSystemResponse3.png

@section page_ctrl_src Source Code Access
Source Code related to Lab 0x06 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%206/

@section page_ctrl_doc Documentation
Documentation related to Lab 0x06 is located at \ref UIFrontEnd.py and \ref main.py

@page page_trac Reference Tracker (Lab 0x07)

@section page_trac_desc Description
The Reference Tracker takes the same code from \ref page_ctrl, and updates it
to execute control over changing velocity values. It includes \ref UIFrontEnd.py 
code to execute in Spyder and prompt user commands and \ref main.py code that 
execultes on the Nucleo. It also outputs a plot and CSV file of the data once 
finished.

Again, motor assembly friction and imbalance has led this code to not execute 
as reliably as intended. The response of the motor, as a result, is seemingly 
random and Kp adjustments have very little effect on the data.

@image html TrackingPlot.png

@section page_trac_src Source Code Access
Source Code related to Lab 0x07 is located at: https://bitbucket.org/slavickju/mechatronics_lab/src/master/Lab%207/

@section page_trac_doc Documentation
Documentation related to Lab 0x07 is located at \ref UIFrontEnd.py and \ref main.py
'''