'''
@file VirtualLED.py

The example will implement some code to control an imaginary LED using a finite
state machine. The LED will automatically flicker between an ON and OFF state
'''

from random import choice
import time

class TaskVirtualLED:
    '''
    @brief      A finite state machine to control LED.
    @details    This class implements a finite state machine to control the
                operation of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_LED_ON  = 1    
    
    ## Constant defining State 2
    S2_LED_OFF = 2    

    
    def __init__(self, interval):
        '''
        @brief            Creates a TaskVirtualLED object.
        
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_LED_ON)
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_LED_ON):
                # Run State 1 Code
                self.transitionTo(self.S2_LED_OFF)
                print(str(self.runs) + ': State 1: ' + 'LED ON ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_LED_OFF):
                # Run State 2 Code
                self.transitionTo(self.S1_LED_ON)
                print(str(self.runs) + ': State 2 ' + 'LED OFF ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

# class Button:
#     '''
#     @brief      A pushbutton class
#     @details    This class represents a button that the can be pushed by the
#                 imaginary driver to turn on or off the wipers. As of right now
#                 this class is implemented using "pseudo-hardware". That is, we
#                 are not working with real hardware IO yet, this is all pretend.
#     '''
    
#     def __init__(self, pin):
#         '''
#         @brief      Creates a Button object
#         @param pin  A pin object that the button is connected to
#         '''
        
#         ## The pin object used to read the Button state
#         self.pin = pin
        
#         print('Button object created attached to pin '+ str(self.pin))

    
#     def getButtonState(self):
#         '''
#         @brief      Gets the button state.
#         @details    Since there is no hardware attached this method
#                     returns a randomized True or False value.
#         @return     A boolean representing the state of the button.
#         '''
#         return choice([True, False])

# class MotorDriver:
#     '''
#     @brief      A motor driver.
#     @details    This class represents a motor driver used to make the wipers
#                 wipe back and forth.
#     '''
    
#     def __init__(self):
#         '''
#         @brief Creates a MotorDriver Object
#         '''
#         pass
    
#     def Forward(self):
#         '''
#         @brief Moves the motor forward
#         '''
#         print('Motor moving CCW')
    
#     def Reverse(self):
#         '''
#         @brief Moves the motor forward
#         '''
#         print('Motor moving CW')
    
#     def Stop(self):
#         '''
#         @brief Moves the motor forward
#         '''
#         print('Motor Stopped')



























