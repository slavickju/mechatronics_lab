# -*- coding: utf-8 -*-
'''
@file UIFrontEnd.py

This file reads commands from the console and writes them to the Nucleo for it
to record encoder data. It then reads the results from the Nucleo and creates
a plot and a CSV file of the data.

@author: justi
'''

import serial
import array
import time
import csv
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np

# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     return myval

# for n in range(1):
#     print(sendChar())
    
# ser.close()

S0_INIT = 0
S1_LIST_OF_COMMANDS = 1
S2_USER_INPUT = 2
S3_ENCODER_RESPONSE = 3
state = S0_INIT
runs = 0
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
writer = csv.writer(open('Lab4Data.csv', 'w', newline=''))

while(True):
    if(state == S0_INIT):
        my_arr = array.array('I', [])
        data = array.array('I', [])
        time_array = array.array('f', []);
        ## Initializes the serial port
        #self.ser.init(9600, bits=8, parity=None, stop=1)
        state = S1_LIST_OF_COMMANDS
                 
    elif(state == S1_LIST_OF_COMMANDS):
        # Run State 1 Code
        inv = 'S'
        print('List of Commands: ')
        print('G: Begin Sampling from Encoder')
        print('S: Stop Collection')
        print(' ')
        inv = input('Enter your Command: ')
        state = S2_USER_INPUT
        #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
    elif(state == S2_USER_INPUT):
        # Run State 2 Code
        ser.write(str(inv).encode('ascii'))
        state = S3_ENCODER_RESPONSE
        
    elif(state == S3_ENCODER_RESPONSE):
        # Run State 3 Code
        if (inv == 'S'):
            myval = ser.readline().decode('ascii')
            my_arr = myval.split(',')
            print(my_arr)
            array_size = my_arr.index('')
            my_arr.pop()
            for i in range(1,array_size):
                data.append(int(my_arr.pop()))
                time_array.append(i/5)
            data.reverse()
            data = data.tolist()
            time_array = time_array.tolist()
            plt.plot(time_array, data)
            plt.show()
            
            time_stamp = time.ctime()
            print(time)
            data.insert(0, time_stamp)
            writer.writerow(data)
            
        state = S0_INIT
        # if (myval != ''):
        #     my_arr.append(int(myval))
        #     time += (1/5)
        #     time_array.append(time)
        # if (inv == 'S'):
        #     fig, ax = plt.subplots()  # Create a figure containing a single axes.
        #     ax.plot(time_array, my_arr)  # Plot some data on the axes.
        #     my_arr = array.array('I', [])
        #     time_array = array.array('I', [])
        #     time = 0;
        # state = S1_LIST_OF_COMMANDS
        # fig, ax = plt.subplots()
        
        
        # if (myval != ''):
        #     state = S1_LIST_OF_COMMANDS
        # else:
        #     state = S3_ENCODER_RESPONSE
        
        
    runs += 1
            
        

                
        
