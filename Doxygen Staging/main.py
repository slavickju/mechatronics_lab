'''
@file main.py

This file reads the encoder position once the command to start is recieved,
stops recording upon the stop command, writes the data to the REPL, and
executes automatically upon booting up the port. 

'''
import pyb
from pyb import UART
import utime
myuart = UART(2)

interval = 200000   
start_time = utime.ticks_us()     
## The "timestamp" for when the task should run next
next_time = utime.ticks_add(start_time, interval)

pin1 = pyb.Pin.cpu.A6
pin2 = pyb.Pin.cpu.A7
timer = 3
tim = pyb.Timer(timer, prescaler=0, period=0xFFFF)
tim.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
tim.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)

collect_data = False
array_length = -1
start_list_time = utime.ticks_us()

S0_INIT = 0
S1_READ_INPUT = 1
S2_RECORD_DATA = 2
state = S0_INIT

while True:
    curr_time = utime.ticks_us()
    if (utime.ticks_diff(curr_time, next_time) >= interval):
        if (state == S0_INIT):
            state = S1_READ_INPUT
        
        if (state == S1_READ_INPUT):
            if (myuart.any() != 0):
                val = myuart.readchar()
                if (val == 0x47):
                    # myuart.write('Started Data Collection ')
                    collect_data = True
                    start_list_time = curr_time
                elif (val == 0x53):
                    # myuart.write('Stopped Data Collection ')
                    start_list_time = curr_time
                    collect_data = False
               
            if (utime.ticks_diff(curr_time, start_list_time) >= 1e7):
                start_list_time = curr_time
                collect_data = False
            if(collect_data): 
                state = S2_RECORD_DATA
        
        if (state == S2_RECORD_DATA):
            myuart.write(str(tim.counter())+',')
            state = S1_READ_INPUT
            
        next_time = utime.ticks_add(next_time, interval)
    
# from pyb import UART
# myuart = UART(2)
# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
#         myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo')