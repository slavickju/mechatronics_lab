'''
@file Encoder.py

This file creates an EncoderDriver class that can read and update the encoder 
position and an EncoderInterface class that can read commands from the console 
and communicate with the EncoderDriver class to display encoder information.

Created on Tue Oct 22 01:30:00 2020

@author Justin Slavick

@copyright License Info

@date October 22, 2020
'''

import pyb
import utime
import Lab3Shares

## An encoder driver object
class EncoderDriver:
    
    ## Constant defining State 1
    S1_UPDATE  = 1    

    def __init__(self, pin1, pin2, timer, interval):
        '''
        @brief Creates a timer in encoder mode.
        
        @details Inputs two pin locations in the form of a string and a timer 
        in the form of an integer. This method uses these values to set up a 
        Timer in encoder mode using the specified int timer value. Also
        creates two stored_timer variables that store the two most recent
        updated timer counter values. There are two in order to compare the 
        delta and prevent overflow and underflow.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S1_UPDATE
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.tim = pyb.Timer(timer, prescaler=0, period=0xFFFF)
        self.tim.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.position = 0
        
    def run(self):
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S1_UPDATE):
                # Run State 1 Code
                if(Lab3Shares.CMD != None):
                    if(Lab3Shares.CMD == 0x7A):
                        self.set_position(0)  
                    elif(Lab3Shares.CMD == 0x70):
                        print(self.get_position())   
                    elif(Lab3Shares.CMD == 0x64):
                        print(self.get_delta())
                    else:
                        pass
                    
                    Lab3Shares.RESP = Lab3Shares.CMD
                    Lab3Shares.CMD = None
                    
                self.update()
                    
            else:
                # Invalid state code (error handling)
                pass
            
        self.runs += 1
            
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def update(self):
        '''
        @brief Updates the encoder's position and corrects for over/underflow.
        
        @details Changes the value of stored_timer to that currently outputted
        by the pyb Timer class's counter() method. Also checks for overflow
        or underflow. If the magnetude of delta is greater than half the 
        period, it corrects the value by adding or subtracting the period,
        depending on if delta is positive or negative.
        '''
        self.stored_timer2 = self.stored_timer1
        self.stored_timer1 = self.tim.counter()
        if (abs(self.get_delta()) > 0xFFFF/2):
            if(self.get_delta() < 0):
                self.position += self.set_delta(0xFFFF)
            else:
                self.position += self.set_delta(-0xFFFF)
        else:
            self.position += self.get_delta()
        
    def get_position(self):
        '''
        @brief Gets the encoder's most recent stored position.
        
        @details Returns the value of stored_timer1.
        '''
        return self.position

    def set_position(self, position):
        '''
        @brief Changes the encoder's position and updates the stored_encoder
        value
        
        @details Changes the value of pyb Timer's counter method to the value 
        given in position. It then calls the update method to update the
        stired timer value. Mostly used to set the encoder position to zero.
        It also resets both stored_timer values to avoid outputting a delta
        due to recalibration.
        '''
        self.tim.counter(position)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.update()
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent stored
        timer values
        
        @details Finds the difference between stored_timer1 and stored_timer2
        in order to find if there is. overflow or underflow in the encoder.
        This value is returned
        '''
        return (self.stored_timer1 - self.stored_timer2)
    
    def set_delta(self, value):
        '''
        @brief Changes the delta to a different value
        
        @details Returns delta added with another value initialized in the 
        method. The main use of this is to correct "bad" delta values by
        adding or subtracting the period.
        '''
        return (self.get_delta() + value)

## An encoder interface object
class EncoderInterface:
      
    ## Constant defining State 0
    S0_INIT = 0
    
    ## Constant defining State 1
    S1_LIST_OF_COMMANDS  = 1   
    
    ## Constant defining State 2
    S2_USER_INPUT = 2
    
    def __init__(self, interval):
        
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                 ## The serial port that reads ASCII characters
                 self.ser = pyb.UART(2)
                 
                 ## Initializes the serial port
                 #self.ser.init(9600, bits=8, parity=None, stop=1)
                 self.transitionTo(self.S1_LIST_OF_COMMANDS)
                 
            elif(self.state == self.S1_LIST_OF_COMMANDS):
                # Run State 1 Code
                print('List of Commands: ')
                print('z: Zeros out the Encoder Position')
                print('p: Prints the Encoder Position')
                print('d: Prints the Encoder Delta')
                print(' ')
                
                self.transitionTo(self.S2_USER_INPUT)
                #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_USER_INPUT):
                # Run State 2 Code
                if (self.ser.any() > 0):
                    Lab3Shares.CMD = self.ser.readchar()
                    
                if (Lab3Shares.RESP != None):
                    self.transitionTo(self.S1_LIST_OF_COMMANDS)
                    Lab3Shares.RESP = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
        self.runs += 1
            
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
                