'''
@file Lab0x03_main.py

This file allows the execution and multitasking between the EncoderDriver and 
EncoderInterface class.

Created on Tue Oct 22 01:30:00 2020
'''
import pyb
from Encoder import EncoderDriver, EncoderInterface


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
# Creating a task object using the virtual and physical LED objects above
pin1 = pyb.Pin.cpu.A6
pin2 = pyb.Pin.cpu.A7
interval = 2925714
task1 = EncoderDriver(pin1, pin2, 3, interval)
task2 = EncoderInterface(interval)

# Run the tasks in sequence over and over again
for N in range(3000000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()