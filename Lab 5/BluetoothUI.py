
'''
@file BluetoothUI.py

Creates a BLEModuleDriver object and uses it and commands from the DSD TECH
app to cause the user LED to flicker at user specified frequencies.

Created on Tue Nov 24 01:45:41 2020

@author: justi
'''

import pyb
import utime
from pyb import UART
## An object that can read and interpret values sent to the Nucleo over
## bluetooth
class BLEModuleDriver:  

    def __init__(self, pin):
        '''
        @brief Sets up the LED pin and bluetooth UART communication.
        '''
        self.pinA5 = pyb.Pin(pin, pyb.Pin.OUT_PP)
        self.uart = UART(3, 9600)
        self.LEDOn = False
    
    def isOn(self):
        '''
        @brief Returns a boolean confirming that the LED is not turned on
        '''
        return self.LEDOn
        
    
    def turnOn(self):
        '''
        @brief Turns on user LED
        
        @details Turns on the user LED by implementing the Pin.high() method.
        '''
        self.pinA5.high()
        self.LEDOn = True
        
    def turnOff(self):
        '''
        @brief Turns off user LED
        
        @details Turns off the user LED by implementing the Pin.low() method.
        '''
        self.pinA5.low()
        self.LEDOn = False
        
    def checkChar(self):
        '''
        @brief Checks to see if there is a value sent to the REPL
        
        @details Uses uart.any() to see if there is a value printed to the
        REPL. Returns a boolean depending on the result.
        '''
        return (self.uart.any() != 0)
            
        
    def readChar(self):
        '''
        @brief Reads the value on the REPL and returns it if it is an int
        
        @details Converts the read value to an int and returns it. It will 
        return None if the value is not an int.
        '''
        try:
            val = int(self.uart.readline())
            return val
        except ValueError:
            return None
        
    def writeChar(self, char):
        '''
        @brief Reads the value on the REPL and returns it if it is an int
        
        @details Converts the read value to an int and returns it. It will 
        return None if the value is not an int.
        '''
        self.uart.write(char)

       
if (__name__ =='__main__'):
    ## Counter that describes the number of times the task has run
    runs = 0
        
    ##  The amount of time in seconds between runs of the task
    interval = 1000000
        
    ## The timestamp for the first iteration
    start_time = utime.ticks_us()
        
    ## The "timestamp" for when the task should run next
    next_time = utime.ticks_add(start_time, interval)
        
    pinA5 = pyb.Pin.cpu.A5
    driver = BLEModuleDriver(pinA5)
    
    flicker = False
    flic_time = 0

    while(True):
        curr_time = utime.ticks_us()
        if (utime.ticks_diff(curr_time, next_time) >= 0):
            if(driver.checkChar()):
                val = driver.readChar()
                if (val != None):
                    flicker = True
                    flic_time = curr_time
                    driver.writeChar('LED Blinking at ' + str(val) + ' Hz.')
            if (flicker):
                if (utime.ticks_diff(curr_time,flic_time) > 1e6/val):
                    if (driver.isOn()):
                        driver.turnOff()
                    else:
                        driver.turnOn()
                    flic_time = curr_time
            
            runs += 1
            
            # Specifying the next time the task will run
            next_time = utime.ticks_add(next_time, interval)

