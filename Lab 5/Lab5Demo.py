# -*- coding: utf-8 -*-
'''
@file Lab5Demo.py

Tests the functionality of the BLE Antenna and the DSD TECH app.

Created on Mon Nov 16 19:18:12 2020

@author: justi
'''

import pyb
from pyb import UART
uart = UART(3, 9600)
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val,' turns it OFF')
            pinA5.low()
        elif val == 1:
            print(val,' turns it ON')
            pinA5.high()