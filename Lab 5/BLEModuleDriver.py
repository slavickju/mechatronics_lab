
'''
@file BLEModuleDriver.py

Establishes a BLEModuleDriver class that communicates with the Nucelo REPL in
response to commands from the DSD TECH App. Also has methods that can directly
turn on and off the user LED on the Nucleo.

Created on Tue Nov 24 01:04:21 2020

@author: justi
'''
import pyb
from pyb import UART

## An object that can read and interpret values sent to the Nucleo over
## bluetooth
class BLEModuleDriver:  

    def __init__(self, pin):
        '''
        @brief Sets up the LED pin and bluetooth UART communication.
        '''
        self.pinA5 = pyb.Pin(pin, pyb.Pin.OUT_PP)
        self.uart = UART(3, 9600)
        self.LEDOn = False
    
    def isOn(self):
        '''
        @brief Returns a boolean confirming that the LED is not turned on
        '''
        return self.LEDOn
        
    
    def turnOn(self):
        '''
        @brief Turns on user LED
        
        @details Turns on the user LED by implementing the Pin.high() method.
        '''
        self.pinA5.high()
        self.LEDOn = True
        
    def turnOff(self):
        '''
        @brief Turns off user LED
        
        @details Turns off the user LED by implementing the Pin.low() method.
        '''
        self.pinA5.low()
        self.LEDOn = False
        
    def checkChar(self):
        '''
        @brief Checks to see if there is a value sent to the REPL
        
        @details Uses uart.any() to see if there is a value printed to the
        REPL. Returns a boolean depending on the result.
        '''
        return (self.uart.any() != 0)
            
        
    def readChar(self):
        '''
        @brief Reads the value on the REPL and returns it if it is an int
        
        @details Converts the read value to an int and returns it. It will 
        return None if the value is not an int.
        '''
        try:
            val = int(self.uart.readline())
            return val
        except ValueError:
            return None
        
    def writeChar(self, char):
        '''
        @brief Reads the value on the REPL and returns it if it is an int
        
        @details Converts the read value to an int and returns it. It will 
        return None if the value is not an int.
        '''
        self.uart.write(char)

       