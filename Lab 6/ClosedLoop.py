# -*- coding: utf-8 -*-
'''
@file ClosedLoop.py

Created on Wed Nov 25 00:42:45 2020

@author: justi
'''
class ClosedLoop:
    ''' 
    This class is in charge of defining the closed-loop transfer function, 
    changing the value of the controller, and saturation limits of the PWM. 
    '''    
    def __init__ (self, ref, init_gain, CPR):
        ''' 
        Creates a motor driver by initializing GPIO12pins and turning the motor 
        off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. 
        '''
        print ('Creating a motor driver')
        self.Kp = init_gain
        
        self.Omega_ref = ref
        self.Omega_meas = 0
        self.Omega_error = (self.Omega_ref - self.Omega_meas)*self.Kp
        
        self.PWM_max = 100
        self.PWM_min = 20
    
    def update (self):
        print ('Enabling Motor')
        self.Omega_error = (self.Omega_ref - self.Omega_meas)*self.Kp
        if (self.Omega_error > self.PWM_max):
            self.Omega_error = self.PWM_max
        if (self.Omega_error < self.PWM_min):
            self.Omega_error = self.PWM_min
        
        
    def get_Kp (self):
        print ('Disabling Motor')
        return self.Kp
        
    def set_Kp (self, Kp):
        ''' 
        This method sets the duty cycle to be sent to the motor to the given 
        level. Positive values cause effort in one direction, negative values 
        in the opposite direction.
        
        @param duty A signed integer holding the duty cycle of the PWM signal 
        sent to the motor 
        '''
        self.Kp = Kp
        
    def set_Omega_meas (self, omega):
        self.Omega_meas = omega
