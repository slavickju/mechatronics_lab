# -*- coding: utf-8 -*-
'''
@file UIFrontEnd.py

This file reads commands from the console and writes them to the Nucleo for it
to record encoder data. It then reads the results from the Nucleo and creates
a plot and a CSV file of the data.

@author: justi
'''

import serial
import array
import time
import csv
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     return myval

# for n in range(1):
#     print(sendChar())
    
# ser.close()

S0_INIT = 0
S1_LIST_OF_COMMANDS = 1
S2_USER_INPUT = 2
S3_ENCODER_RESPONSE = 3
state = S0_INIT
runs = 0
Proceed = True
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
writer = csv.writer(open('Lab6Data.csv', 'w', newline=''))

while(Proceed):
    if(state == S0_INIT):
        my_arr = array.array('f', [])
        data = array.array('f', [])
        time_array = array.array('f', []);
        omega = array.array('f', [])
        ## Initializes the serial port
        #self.ser.init(9600, bits=8, parity=None, stop=1)
        state = S1_LIST_OF_COMMANDS
                 
    elif(state == S1_LIST_OF_COMMANDS):
        # Run State 1 Code
        print('List of Commands: ')
        print('G: Start Control System')
        print('S: Stop Control System')
        print('Q: Quit')
        print(' ')
        inv = input('Enter your Command: ')
        if (inv == 'G'):
            print(' ')
            Kp = input('Enter Kp Value: ')
            state = S2_USER_INPUT
        elif (inv == 'S'):
            state = S3_ENCODER_RESPONSE
        elif (inv == 'Q'):
            Proceed = False
        else:
            print('Invalid Response, Try Again')
            print(' ')
        #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
    elif(state == S2_USER_INPUT):
        # Run State 2 Code
        ser.write(str(Kp).encode('ascii'))
        state = S1_LIST_OF_COMMANDS
        
    elif(state == S3_ENCODER_RESPONSE):
        # Run State 3 Code
        if (inv == 'S'):
            myval = ser.readline().decode('ascii')
            my_arr = myval.split(',')
            print(my_arr)
            array_size = my_arr.index('')
            my_arr.pop()
            for i in range(1,array_size):
                data.append(float(my_arr.pop()))
                time_array.append(i/5)
                omega.append(1000)
            data.reverse()
            data = data.tolist()
            time_array = time_array.tolist()
            fig = plt.figure()
            ax1 = fig.add_subplot(211)
            ax1.set_xlabel('Time (s)')
            ax1.set_ylabel('Motor Angular Velocity (RPM)')
            ax1.set_title('Control System Response')
            plt.plot(time_array, data)
            plt.plot(time_array, omega, 'r')
            
            time_stamp = time.ctime()
            data.insert(0, time_stamp)
            print(data)
            writer.writerow(data)
            
        state = S0_INIT
        # if (myval != ''):
        #     my_arr.append(int(myval))
        #     time += (1/5)
        #     time_array.append(time)
        # if (inv == 'S'):
        #     fig, ax = plt.subplots()  # Create a figure containing a single axes.
        #     ax.plot(time_array, my_arr)  # Plot some data on the axes.
        #     my_arr = array.array('I', [])
        #     time_array = array.array('I', [])
        #     time = 0;
        # state = S1_LIST_OF_COMMANDS
        # fig, ax = plt.subplots()
        
        
        # if (myval != ''):
        #     state = S1_LIST_OF_COMMANDS
        # else:
        #     state = S3_ENCODER_RESPONSE
        
        
    runs += 1
            
        

                
        
