# -*- coding: utf-8 -*-
'''
@file ControlSystem.py

Created on Wed Nov 25 00:44:27 2020

@author: justi
'''

import pyb
import utime

class ControlSystem:
    
    def __init__ (self, mot, enc, loop, interval):
        
        self.motor = mot
        self.encoder = enc
        self.closed_loop = loop
        
        self.Kp = self.closed_loop.get_Kp()
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run (self):
        run_time = utime.ticks_us()
        curr_time = run_time
        if (utime.ticks_diff(curr_time, self.next_time) >= 0):
            self.encoder.update()
            self.closed_loop.set_Omega_meas(self.encoder.getPosition())
            error = self.closed_loop.update()
            self.motor.set_duty(error, True)
            self.motor.enable()
            
        self.runs += 1
            
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    
    
# if (__name__ =='__main__'):
#     # Adjust the following code to write a test program for your motor 
#     # class. Any code within the if __name__ == '__main__' block will 
#     # only run when the script is executed as a standalone program. If 
#     # the script is imported as a module the code block will not run.
            
#     # Create the pin objects used for interfacing with the motor driver
#     pin_nSLEEP = pyb.Pin.cpu.A15
#     pin_IN1 = pyb.Pin.cpu.B4
#     pin_IN2 = pyb.Pin.cpu.B5
    
    
#     # Create a motor object passing in the pins and timer
#     moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, 3)
    
#     pin1 = pyb.Pin.cpu.B6
#     pin2 = pyb.Pin.cpu.B7
#     encoder = EncoderDriver(pin1, pin2, 4, 200000)
    
#     closed_loop = ClosedLoop(100, 0.5, 1000)
    
#     cs = ControlSystem(moe, encoder, closed_loop)
    
#     for n in range(1,100):
#         cs.run()