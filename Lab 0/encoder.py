## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Justin Slavick
#
#  @copyright License Info
#
#  @date October 22, 2020
#
#  @package encoder


import pyb

## An encoder driver object
class EncoderDriver:

    def __init__(self, pin1, pin2, timer):
        '''
        @brief Creates a timer in encoder mode.
        
        @details Inputs two pin locations in the form of a string and a timer 
        in the form of an integer. This method uses these values to set up a 
        Timer in encoder mode using the specified int timer value. Also
        creates two stored_timer variables that store the two most recent
        updated timer counter values. There are two in order to compare the 
        delta and prevent overflow and underflow.
        '''
        self.tim = pyb.Timer(timer, prescaler=0, period=0xFFFF)
        self.tim.channel(1, pin=pyb.Pin.cpu.pin1, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pyb.Pin.cpu.pin2, mode=pyb.Timer.ENC_AB)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.position = 0
        
    def update(self):
        '''
        @brief Updates the encoder's position and corrects for over/underflow.
        
        @details Changes the value of stored_timer to that currently outputted
        by the pyb Timer class's counter() method. Also checks for overflow
        or underflow. If the magnetude of delta is greater than half the 
        period, it corrects the value by adding or subtracting the period,
        depending on if delta is positive or negative.
        '''
        self.stored_timer2 = self.stored_timer1
        self.stored_timer1 = self.tim.counter()
        if (abs(self.get_delta()) > 0xFFFF/2):
            if(self.get_delta() < 0):
                self.position += self.set_delta(0xFFFF)
            else:
                self.position += self.set_delta(-0xFFFF)
        
    def get_position(self):
        '''
        @brief Gets the encoder's most recent stored position.
        
        @details Returns the value of stored_timer1.
        '''
        return self.stored_timer1

    def set_position(self, position):
        '''
        @brief Changes the encoder's position and updates the stored_encoder
        value
        
        @details Changes the value of pyb Timer's counter method to the value 
        given in position. It then calls the update method to update the
        stired timer value.
        '''
        self.tim.counter(position)
        self.update()
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent stored
        timer values
        
        @details Finds the difference between stored_timer1 and stored_timer2
        in order to find if there is. overflow or underflow in the encoder.
        This value is returned
        '''
        return (self.stored_timer1 - self.stored_timer2)
    
    def set_delta(self, value):
        '''
        @brief Changes the delta to a different value
        
        @details Returns delta added with another value initialized in the 
        method. The main use of this is to correct "bad" delta values by
        adding or subtracting the period.
        '''
        return (self.get_delta() + value)