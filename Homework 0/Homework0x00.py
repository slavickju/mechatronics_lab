'''
@file Homework0x00.py

The example will implement some code to control an imaginary elevator using a
finite state machine.

The user has a button to move the elevator to floor 1, and a button to move the elevator to floor 2.

There is also a sensor at either each floor to tell whether the elevator is there or not.

Created on Tue Oct 6 04:43:00 2020
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP = 2    
    
    ## Constant defining State 3
    S3_STOPPED_ON_FLOOR_1     = 3    
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    def __init__(self, interval, Button_1, Button_2,  First, Second, Motor):
        '''
        @brief            Creates a TaskElevator object.
        @param button_1   An object from class Button representing the first floor button
        @param button_2   An object from class Button representing the second floor button
        @param first      An object from class Button representing the floor 1 sensor
        @param second     An object from class Button representing the floor 2 sensor.
        @param motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the first floor button
        self.button_1 = Button_1
        
        ## The button object used for the second floor button
        self.button_2 = Button_2
        
        ## The button object used for the floor one sensor
        self.first = First
        
        ## The button object used for the floor two sensor
        self.second = Second
        
        ## The motor object "moving" the elevator
        self.motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.setMotorValue(2)
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                
                if(self.runs*self.interval > 10):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                
                if(self.first.getButtonState() == 1):
                    self.transitionTo(self.S3_STOPPED_ON_FLOOR_1)
                    self.motor.setMotorValue(0)
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_ON_FLOOR_1):
                # Run State 2 Code
                if(self.button_2.getButtonState() == 1):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.motor.setMotorValue(1)
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_MOVING_UP):
                # Transition to state 1 if the left limit switch is active
                if(self.second.getButtonState() == 1):
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    self.motor.setMotorValue(0)
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                # Run State 4 Code
                if(self.button_1.getButtonState() == 1):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.motor.getMotorValue(2)
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return self.setButtonState()
    
    def setButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([0, 1])

class MotorDriver:
    '''
    @class MotorDriver
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def setMotorValue(self, value):
        '''
        @brief Sets the Direction of the Motor
        '''
        if value == 0:
            print('Motor Stopped (motor = 0)')
            return 0
        elif value == 1:
            print('Motor moving up (motor = 1)')
            return 1
        elif value == 2:
            print('Motor moving down (motor = 2)')
        else:
             return motor.getMotorValue  
        
    def getMotorValue(self):
        '''
        @brief Returns the current value of motor
        '''
        return self
        
    def Up(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving up (motor = 1)')
    
    def Down(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving down (motor = 2)')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped (motor = 0)')



























