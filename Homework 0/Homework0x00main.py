'''
@file Homework0x00main.py

This file executes the TaskElevator class and creates various objects
representing the different Elevator buttons. Also has multitasking 
capabilities.

Created on Tue Oct 6 04:43:00 2020

'''

from Homework0x00 import Button, MotorDriver, TaskElevator


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
Button_1 = Button('PB5')
Button_2 = Button('PB6')
First = Button('PB7')
Second = Button('PB8')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, Button_1, Button_2, First, Second, Motor)
# task2 = TaskElevator(0.1, Button_1, Button_2, First, Second, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
#    task2.run()
#    task3.run()