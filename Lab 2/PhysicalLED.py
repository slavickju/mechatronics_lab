'''
@file PhysicalLED.py

The example will implement some code to control the LED on the Nucleo
control board using a finite state machine. The LED will automatically 
oscillate between 0% and 100% brightness with awavelength of 10 seconds. It 
will transition between a sinusoidal and triangle waveform every 30 seconds.
'''

from random import choice
import utime
import math
import pyb

class TaskPhysicalLED:
    '''
    @brief      A finite state machine to control LED.
    @details    This class implements a finite state machine to control the
                operation of the LED on the Nucleo control board.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_SINE_WAVE  = 1    
    
    ## Constant defining State 2
    S2_TRIANGLE_WAVE = 2    

    
    def __init__(self, interval):
        '''
        @brief            Creates a TaskPhyscialLED object.
        
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        self.tim2 = pyb.Timer(2, freq = 20000)
        
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_SINE_WAVE)
                #print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_SINE_WAVE):
                # Run State 1 Code
                self.t2ch1.pulse_width_percent(50*(math.sin((utime.ticks_diff(self.curr_time, self.start_time))*(2*math.pi/10000))) + 50)
                if((utime.ticks_diff(self.curr_time, self.start_time))%30000 == 0): #& ((self.curr_time-self.start_time) < (self.next_time-self.curr_time + 30)):
                    self.transitionTo(self.S2_TRIANGLE_WAVE)
                    
            elif(self.state == self.S2_TRIANGLE_WAVE):
                # Run State 2 Code
                self.t2ch1.pulse_width_percent(0.01*((utime.ticks_diff(self.curr_time, self.start_time)) % 10000))
                if((utime.ticks_diff(self.curr_time,self.start_time))%30000 == 0): #& ((self.curr_time-self.start_time) < (self.next_time-self.curr_time + 30)):
                    self.transitionTo(self.S1_SINE_WAVE)
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

# class Button:
#     '''
#     @brief      A pushbutton class
#     @details    This class represents a button that the can be pushed by the
#                 imaginary driver to turn on or off the wipers. As of right now
#                 this class is implemented using "pseudo-hardware". That is, we
#                 are not working with real hardware IO yet, this is all pretend.
#     '''
    
#     def __init__(self, pin):
#         '''
#         @brief      Creates a Button object
#         @param pin  A pin object that the button is connected to
#         '''
        
#         ## The pin object used to read the Button state
#         self.pin = pin
        
#         print('Button object created attached to pin '+ str(self.pin))

    
#     def getButtonState(self):
#         '''
#         @brief      Gets the button state.
#         @details    Since there is no hardware attached this method
#                     returns a randomized True or False value.
#         @return     A boolean representing the state of the button.
#         '''
#         return choice([True, False])

# class MotorDriver:
#     '''
#     @brief      A motor driver.
#     @details    This class represents a motor driver used to make the wipers
#                 wipe back and forth.
#     '''
    
#     def __init__(self):
#         '''
#         @brief Creates a MotorDriver Object
#         '''
#         pass
    
#     def Forward(self):
#         '''
#         @brief Moves the motor forward
#         '''
#         print('Motor moving CCW')
    
#     def Reverse(self):
#         '''
#         @brief Moves the motor forward
#         '''
#         print('Motor moving CW')
    
#     def Stop(self):
#         '''
#         @brief Moves the motor forward
#         '''
#         print('Motor Stopped')



























