'''
@file Lab0x02_main.py

This file allows for the execution and multitasking of the TaskPhysicalLED
class and the TaskVirtualLED class.

Created on Tue Oct 6 04:43:00 2020
'''

from VirtualLED import TaskVirtualLED
from PhysicalLED import TaskPhysicalLED


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
# Creating a task object using the virtual and physical LED objects above
task1 = TaskPhysicalLED(1)
task2 = TaskVirtualLED(1)

# Run the tasks in sequence over and over again
for N in range(3000000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()