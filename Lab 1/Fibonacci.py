# -*- coding: utf-8 -*-
'''
@file Fibonacci.py

This file allows the user to input an index of the Fibonacci sequence and will
output the Fibonacci number at that index in response to the console.

Created on Thu Sep 24 08:38:21 2020

@author: justi
'''

idx = input('What is the index? ')
print('Calculating Fibonacci Number at ''index n = {:}.'.format(idx))
idx = int(idx)

def fib (idx):
    if idx >= 2:
        '''result = fib(idx-1)+fib(idx-2) --- Recursive Method'''
        f0 = 0;
        f1 = 1;
        for i in range(2,idx+1):
            result = f0 + f1
            f0 = f1
            f1 = result
        ''' Iterative Method '''
        return result
    elif idx == 1:
        return 1
    elif idx == 0:
        return 0
    else:
        print('Index not found Error')
    
fibonacci = fib(idx) 
print (fibonacci)   
    
if __name__ == '__main__':
    fib(0)
