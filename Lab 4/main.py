'''
@file main.py

This file reads the encoder position once the command to start is recieved,
stops recording upon the stop command, writes the data to the REPL, and
executes automatically upon booting up the port. 

'''
import pyb
from pyb import UART
import utime
myuart = UART(2)

## An encoder driver object
class EncoderDriver:
    
    ## Constant defining State 1
    S1_UPDATE  = 1    

    def __init__(self, pin1, pin2, timer, interval):
        '''
        @brief Creates a timer in encoder mode.
        
        @details Inputs two pin locations in the form of a string and a timer 
        in the form of an integer. This method uses these values to set up a 
        Timer in encoder mode using the specified int timer value. Also
        creates two stored_timer variables that store the two most recent
        updated timer counter values. There are two in order to compare the 
        delta and prevent overflow and underflow.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S1_UPDATE
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.tim = pyb.Timer(timer, prescaler=0, period=0xFFFF)
        self.tim.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.position = 0
        
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def update(self):
        '''
        @brief Updates the encoder's position and corrects for over/underflow.
        
        @details Changes the value of stored_timer to that currently outputted
        by the pyb Timer class's counter() method. Also checks for overflow
        or underflow. If the magnetude of delta is greater than half the 
        period, it corrects the value by adding or subtracting the period,
        depending on if delta is positive or negative.
        '''
        self.stored_timer2 = self.stored_timer1
        self.stored_timer1 = self.tim.counter()
        if (abs(self.get_delta()) > 0xFFFF/2):
            if(self.get_delta() < 0):
                self.position += self.set_delta(0xFFFF)
            else:
                self.position += self.set_delta(-0xFFFF)
        else:
            self.position += self.get_delta()
        
    def get_position(self):
        '''
        @brief Gets the encoder's most recent stored position.
        
        @details Returns the value of stored_timer1.
        '''
        return self.position

    def set_position(self, position):
        '''
        @brief Changes the encoder's position and updates the stored_encoder
        value
        
        @details Changes the value of pyb Timer's counter method to the value 
        given in position. It then calls the update method to update the
        stired timer value. Mostly used to set the encoder position to zero.
        It also resets both stored_timer values to avoid outputting a delta
        due to recalibration.
        '''
        self.tim.counter(position)
        self.stored_timer1 = 0
        self.stored_timer2 = 0
        self.update()
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent stored
        timer values
        
        @details Finds the difference between stored_timer1 and stored_timer2
        in order to find if there is. overflow or underflow in the encoder.
        This value is returned
        '''
        return (self.stored_timer1 - self.stored_timer2)
    
    def set_delta(self, value):
        '''
        @brief Changes the delta to a different value
        
        @details Returns delta added with another value initialized in the 
        method. The main use of this is to correct "bad" delta values by
        adding or subtracting the period.
        '''
        return (self.get_delta() + value)


if (__name__ =='__main__'):
    interval = 200000   
    start_time = utime.ticks_us()     
    ## The "timestamp" for when the task should run next
    next_time = utime.ticks_add(start_time, interval)

    pin1 = pyb.Pin.cpu.A6
    pin2 = pyb.Pin.cpu.A7
    timer = 3
    tim = pyb.Timer(timer, prescaler=0, period=0xFFFF)
    myEncoder = EncoderDriver(pin1, pin2, timer, interval)
    tim.channel(1, pin=pin1, mode=pyb.Timer.ENC_AB)
    tim.channel(2, pin=pin2, mode=pyb.Timer.ENC_AB)

    collect_data = False
    array_length = -1
    start_list_time = utime.ticks_us()

    S0_INIT = 0
    S1_READ_INPUT = 1
    S2_RECORD_DATA = 2
    state = S0_INIT

    while True:
        curr_time = utime.ticks_us()
        if (utime.ticks_diff(curr_time, next_time) >= interval):
            if (state == S0_INIT):
                state = S1_READ_INPUT
                
            if (state == S1_READ_INPUT):
                if (myuart.any() != 0):
                    val = myuart.readchar()
                    if (val == 0x47):
                        # myuart.write('Started Data Collection ')
                        collect_data = True
                        start_list_time = curr_time
                        myEncoder.set_position(0)
                    elif (val == 0x53):
                        # myuart.write('Stopped Data Collection ')
                        start_list_time = curr_time
                        collect_data = False
               
                if (utime.ticks_diff(curr_time, start_list_time) >= 1e7):
                    start_list_time = curr_time
                    collect_data = False
                if(collect_data): 
                    state = S2_RECORD_DATA
        
            if (state == S2_RECORD_DATA):
                myEncoder.update()
                myuart.write(str(myEncoder.get_position())+',')
                state = S1_READ_INPUT
            
            next_time = utime.ticks_add(next_time, interval)
    