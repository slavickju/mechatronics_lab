# -*- coding: utf-8 -*-
'''
@file Lab4Example.py
Created on Wed Nov  4 18:08:14 2020

@author: justi
'''

import serial
import Lab4Shares
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    if (myval != inv):
        return myval
    else:
        return 'Try Again'

for n in range(1):
    print(sendChar())
    if(Lab4Shares.CMD != None):
        print(Lab4Shares.CMD)
    
ser.close()
